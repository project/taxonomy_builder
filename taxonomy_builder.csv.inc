<?php

/**
 * @file
 * API functions that parse CSV files into taxonomy trees.
 */

/**
 * Returns the total number of lines in a file, FALSE on errors.
 *
 * @param $filename
 *   A string containing the path to the file.
 * @return
 *   An integer containing the total number of lines.
 */
function taxonomy_builder_line_count($filename) {
  if ($data = file($filename)) {
    return count($data);
  }
  return FALSE;
}

/**
 * Wrapper around fgetcsv(), allows developers to get a chunk of data as opposed
 * to reading the entire file.  This allows you to break up the parsing of a
 * file in a batch operation.
 *
 * @param filename
 *   A string containing the path to the file.
 * @param $line
 *   An integer containing the line to start at.  This number is NOT zero-based,
 *   meaning the first line is 1.
 * @param $limit
 *   An integer containing the maximum number of lines to parse, defaults to
 *   0 meaning no limit.
 * @param $length
 *   Must be greater than the longest line (in characters) to be found in the
 *   CSV file (allowing for trailing line-end characters).  Defaults to 4096.
 * @param $delimiter
 *   A string containing the field delimiter, defaults to ','.
 * @param $enclosure
 *   A string containing the field enclosure character, defaults to '"'.
 * @return
 *   An array containing the CSV values.
 */
function taxonomy_builder_fgetcsv($filename, $line, $limit = 0, $length = 4096, $delimiter = ',', $enclosure = '"') {
  $rows = array();
  if (FALSE !== ($handle = fopen($filename, 'r'))) {

    // Skips ahead an $offset number of lines.
    for ($i = 1; $i < $line; $i++) {
      fgets($handle, $length);
    }

    // Parses data from the current pointer.
    for ($i = 0; (!$limit || $i < $limit); $i++) {
      if (FALSE !== ($row = fgetcsv($handle, $length, $delimiter, $enclosure))) {
        $rows[] = $row;
      }
      else {
        break;
      }
    }

    // Closes the pointer.
    fclose($handle);
  }

  // Filters out rows with empty data, returns rows.
  return array_filter($rows, 'taxonomy_builder_filter');
}

/**
 * Filters out rows having any empty values.  Useful as an array_filter()
 * callback.
 *
 * @param $row
 *   An array containing the columns extracted from a row in a CSV file.
 * @return
 *   A boolean flagging whether the row should be stripped.
 */
function taxonomy_builder_filter($row) {
  if (!is_array($row)) {
    return FALSE;
  }
  foreach ($row as $column) {
    if (!drupal_strlen($column)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Extracts columns from a row in a CSV file.
 *
 * @param $row
 *   An array containing a row of columns parsed from a CSV file.
 * @param $columns
 *   An associative array of column numbers in the order they should be
 *   extracted from the line.
 * @return
 *   An array containing the extracted rows.
 */
function taxonomy_builder_extract_columns(array $row, array $columns) {
  $row = array_intersect_key($row, drupal_map_assoc($columns));
  array_multisort($columns, $row);
  return $row;
}

/**
 * Invokes hook_taxonomy_bulder_csv_parser_arguments(), merges defaults and
 * statically caches arguments for performance.
 *
 * @param $vid
 *   An integer containing the vid of the vocabulary the terms will be added to.
 * @param $filename
 *   A string containing the path to the file being parsed.
 * @return
 *   An array of arguments.
 */
function taxonomy_builder_get_parser_arguments($vid, $filename) {
  static $arguments = array();

  // Builds the $key, checks if values are in the static.
  $key = sprintf('%s:%s', $vid, $filename);
  if (!isset($arguments[$key])) {

    // Default values for the parser.
    $default = array(
      'length' => 4096,
      'delimiter' => ',',
      'enclosure' => '"',
    );

    // Gets arguments from hooks, merges defaults and strips invalid keys.
    $arguments[$key] = array_intersect_key($default, array_merge(
      $default,
      module_invoke_all('taxonomy_bulder_csv_parser_arguments', $vid, $filename)
    ));
  }

  // Returns the parser arguments.
  return $arguments[$key];
}

/**
 * Batch function that parses lines from a CSV file into a hierarchical taxonomy
 * tree.
 *
 * @param $vid
 *   An integer containing the vid of the vocabulary the terms will be added to.
 * @param $filename
 *   A a string containing the path to the file being parsed.
 * @param $line
 *   An integer containing the line to start at.  This number is NOT zero-based,
 *   meaning the first line is 1.
 * @param $limit
 *   An integer containing the maximum number of lines to parse at a time,
 *   defaults to 0 meaning no limit.
 * @param &$context
 *   An array containing batch context information.
 * @return
 *   NULL
 */
function taxonomy_builder_batch($vid, $filename, $line, $limit = 0, &$context) {

  // Gets parser arguments from hooks, parses a chunk of the file.
  $args = taxonomy_builder_get_parser_arguments($vid, $filename);
  $rows = taxonomy_builder_fgetcsv(
    $filename, $line, $limit, $args['length'], $args['delimiter'], $args['enclosure']
  );

  // Iterates over the chunk, invokes Taxonomy Builder API hooks.
  foreach ($rows as $row) {
    $altered_row = $row;
    drupal_alter('taxonomy_builder_csv_row', $altered_row, $filename, $vid);
    $tid = taxonomy_builder_save_terms($vid, $altered_row);
    module_invoke_all('taxonomy_builder_csv_row_save', $row, $altered_row, $filename, $vid, $tid);
  }
}
