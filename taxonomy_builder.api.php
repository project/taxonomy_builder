<?php

/**
 * @file
 * Hook definitions for the "Taxonomy Builder API" module.
 */

/**
 * Allows modules to alter the $form_values array prior to it being passed to
 * the taxonomy_save_term() function.  Use hook_taxonomy() in favor of this hook
 * unless you only want the code to be executed when terms are added via
 * Taxonomy Builder API functions or require access to the $hierarchy and/or
 * $level variables.  Note that this hook is invoked before hook_taxonomy().
 *
 * @param $form_values
 *   An array containing the form values that will be passed to the
 *   taxonomy_save_term() function.
 * @param $hierarchy
 *   An array of term names or $form_values arrays in hierarchical order.
 * @param $level
 *   An integer containing the zero-based hierarchical level, 0 being the top
 *   level in the hierarchy.
 * @return
 *   NULL
 */
function hook_taxonomy_builder_term_alter(&$form_values, $hierarchy, $level) {

  // If you are creating a country -> state -> city hierarchy and are positive
  // that all $hierarchy arrays will have three levels, you can automatically
  // set the term description based on the $level of the term being added.
  switch ($level) {
    case 0:
      $form_values['description'] = 'Country';
      break;
    case 1:
      $form_values['description'] = 'State';
      break;
    case 2:
      $form_values['description'] = 'City';
      break;
  }
}

/**
 * Allows modules to take some action after a term has been saved via the
 * taxonomy_save_term() function.  Use hook_taxonomy() in favor of this hook
 * unless you only want the code to be executed when terms are added via
 * Taxonomy Builder API functions or require access to the $hierarchy and/or
 * $level variables.  Note that this hook is invoked after hook_taxonomy().
 *
 * @param $form_values
 *   An array containing the form values that were passed to and returned by the
 *   taxonomy_save_term() function.
 * @param $hierarchy
 *   An array of term names or $form_values arrays in hierarchical order.
 * @param $level
 *   An integer containing the zero-based hierarchical level, 0 being the top
 *   level in the hierarchy.
 * @return
 *   NULL
 */
function hook_taxonomy_builder_term_save($form_values, $hierarchy, $level) {
}

/**
 * Returns arguments to pass to the CSV parser.  Valid array keys are 'length',
 * 'delimiter', and 'enclosure'.  See the taxonomy_builder_fgetcsv() function
 * docblock for what these values mean.  When invoking this hook, use the
 * taxonomy_builder_get_parser_arguments() function.
 *
 * @param $vid
 *   An integer containing the vid of the vocabulary the terms will be added to.
 * @param $filename
 *   A string containing the path to the file being parsed.
 * @return
 *   An array containig the 'length', 'delimiter', and 'enclosure' keys.
 *   Ommitted values inherit taxonomy_builder_fgetcsv() defaults.
 * @see taxonomy_builder_get_parser_arguments()
 */
function hook_taxonomy_bulder_csv_parser_arguments($filename, $vid) {
  // An array with the fgetcsv() defaults.
  return array(
    'length' => 4096,
    'delimiter' => ',',
    'enclosure' => '"',
  );
}

/**
 * Allows modules to alter the $row extracted from the CSV file before it is
 * passed to the taxonomy_builder_save_terms() API function.  The most common
 * use case for this hook is selecting which columns in the row should be used
 * to build the taxonomy hierarchy.
 *
 * @param $row
 *   An array containing a row of columns parsed from a CSV file.
 * @param $filename
 *   A string containing the path to the file being parsed.
 * @param $vid
 *   An integer containing the vid of the vocabulary the terms will be added to.
 * @return NULL
 */
function hook_taxonomy_builder_csv_row_alter(&$row, $filename, $vid) {
  // This example pulls the 5th, 1st, and 3rd columns in that order.
  $row = taxonomy_builder_extract_columns($row, array(4, 0, 2));
}

/**
 * Invoked after $row has been passed to the taxonomy_builder_save_terms()
 * function so that developers can take additional action with the data parsed
 * from the CSV file.  In terms of execution order, this hook is invoked after
 * hook_taxonomy_builder_term_save().
 *
 * @param $row
 *   An array containing a row of columns parsed from a CSV file.
 * @param $altered_row
 *   An array of columns parsed in a CSV line that were altered by the
 *   hook_taxonomy_builder_csv_row_alter() implementation.
 * @param $filename
 *   A string containing the path to the file being parsed.
 * @param $vid
 *   An integer containing the vid of the vocabulary the terms were added to.
 * @param $tid
 *   An integer containing the tid of the last term added from the column.
 */
function hook_taxonomy_builder_csv_row_save($row, $altered_row, $filename, $vid, $tid) {
  // Sticking with our country -> city -> state hierarchy example, we are using
  // this hook to associate a zip code extracted in a column of the CSV file
  // with the $tid of the city added to the database.  For this example, the zip
  // code is in the 4th column of the row.
  if ($tid) {
    db_query(
      "INSERT INTO {zip_codes} (tid, zip_code) VALUES(%d, '%s')",
      array($tid, $row[3])
    );
  }
}
